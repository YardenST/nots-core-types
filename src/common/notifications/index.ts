export interface NotificationButton {
  text: string;
  id: string;
  type: 'button' | 'text';
  placeholder?: string;
}

// todo conditional type
export interface INOPLCommand {
  type: 'dismiss' | 'delay' | 'url' | 'channel' | 'updateNotification' | 'openInApp';
  url?: string;
  channelId?: string;
  interval?: number;
  fieldName?: string;
  fieldValue?: string;
  notificationId?: string;
}

export interface NotificationActions {
  press?: INOPLCommand[];
  dismissed?: INOPLCommand[];
  delivered?: INOPLCommand[];
  actionPress?: { buttonId: string; commands: INOPLCommand[] }[];
}
