export interface BarCodeReadAnalyticsEvent {
  type: 'bar_code_read';
  data: {
    eventData: string;
  };
}

export interface ChannelDeliverySettingsUpdatedAnalyticsEvent {
  type: 'channel_delivery_settings_updated';
  data: {
    channelId: string;
  };
}

export interface ChannelSampleNotificationClickedAnalyticsEvent {
  type: 'channel_sample_notification_clicked';
  data: {
    channelId: string;
  };
}

export interface NotificationsMarkAllReadAnalyticsEvent {
  type: 'notifications_mark_all_read';
  data: {
    unreadCount: number;
  };
}

export interface SwitchToAppAnalyticsEvent {
  type: 'web_switch_to_app';
  data:
    | {
        channelId: string;
        channelName: string;
      }
    | {
        groupId: string;
        groupName: string;
      };
}

export interface DontSwitchToAppAnalyticsEvent {
  type: 'web_dont_switch_to_app';
  data:
    | {
        channelId: string;
        channelName: string;
      }
    | {
        groupId: string;
        groupName: string;
      };
}

export interface ChannelNotificationSettingsChangedAnalyticsEvent {
  type: 'channel_notification_settings_changed';
  data: {
    channelId: string;
    field: 'grouping_selection' | 'override_do_not_disturb_hours' | 'deliver_silently' | 'do_not_disturb_hours';
    value: any;
  };
}

export interface ChannelShareAnalyticsEvent {
  type: 'channel_share';
  data: {
    channelId: string;
    groupId?: string;
    source: 'channel' | 'group' | 'group_channel';
  };
}

export interface ChannelUnsubscribeAnalyticsEvent {
  type: 'channel_unsubscribe';
  data: {
    channelId: string;
    groupId?: string;
    source: 'channel' | 'group' | 'group_channel';
  };
}

export interface ChannelSubscribeAnalyticsEvent {
  type: 'channel_subscribe';
  data: {
    channelId: string;
    groupId?: string;
    source: 'channel_unsubscribe_undo' | 'channel' | 'group' | 'group_channel';
  };
}

export interface NotificationSettingsChangedAnalyticsEvent {
  type: 'notification_settings_changed';
  data: {
    field: 'grouping_selection' | 'override_do_not_disturb_hours' | 'deliver_silently' | 'do_not_disturb_hours';
    value: any;
  };
}

export interface InAppNotificationInteractionAnalyticsEvent {
  type: 'in_app_notification_interaction';
  data: {
    notificationId: string;
    channelId: string;
    type: 'press' | 'action_press';
  };
}

export interface PushNotificationInteractionAnalyticsEvent {
  type: 'push_notification_interaction';
  data: {
    notificationId: string;
    type:
      | 'delivered'
      | 'dismissed'
      | 'press'
      | 'action_press'
      | 'unknown'
      | 'app_blocked'
      | 'channel_blocked'
      | 'channel_group_blocked'
      | 'trigger_notification_created';
  };
}

export interface NotificationListNotificationOpenAnalyticsEvent {
  type: 'notifications_list_notification_open';
  data: {
    notificationId: string;
    channelId: string;
    type: 'openInApp' | 'channel' | 'url' | 'longPress';
  };
}

export interface NotificationOpenChannelAnalyticsEvent {
  type: 'notification_open_channel';
  data: {
    notificationId: string;
    channelId: string;
  };
}

export interface NotificationShareChannelAnalyticsEvent {
  type: 'notification_share';
  data: {
    notificationId: string;
    channelId: string;
  };
}

export interface FeaturedChannelClickAnalyticsEvent {
  type: 'featured_channel_click';
  data: {
    channelId: string;
  };
}

export interface StoreSelectedCategoryAnalyticsEvent {
  type: 'store_selected_category';
  data: {
    category: string;
  };
}

export interface SearchAnalyticsEvent {
  type: 'search';
  data: {
    query: string;
  };
}

export interface SearchPagingAnalyticsEvent {
  type: 'search_paging';
  data: {
    query: string;
    page: number;
  };
}

export interface SearchResultClickAnalyticsEvent {
  type: 'search_result_click';
  data: {
    channelId: string;
  };
}

export interface AppLinkOpenAnalyticsEvent {
  type: 'app_link_open';
  data: {
    resourceId: string;
    resourceType: string;
    webUserID: string | null;
    trackingId?: string | null;
    linkType: 'download' | 'share';
    source: 'branch' | 'clipboard';
    isFirstRun: boolean;
  };
}

export interface AppFirstRunAnalyticsEvent {
  type: 'app_first_run';
  data: {};
}

export interface AppFirstRunAnalyticsEvent {
  type: 'app_first_run';
  data: {};
}

export interface AskNotificationPermissionNativeAnalyticsEvent {
  type: 'ask_notification_permission_native';
  data: {};
}

export interface AskNotificationPermissionFakeAnalyticsEvent {
  type: 'ask_notification_permission_fake';
  data: {};
}

export interface AskNotificationPermissionFakeV1AnalyticsEvent {
  type: 'ask_notification_permission_fake_v1';
  data: {};
}

export interface AskNotificationPermissionFakeV2AnalyticsEvent {
  type: 'ask_notification_permission_fake_v2';
  data: {};
}

export interface WebNotificationsNotSupportedAnalyticsEvent {
  type: 'web_notifications_not_supported';
  data: {};
}

export interface NotificationsPermissionGrantedAnalyticsEvent {
  type: 'notifications_permission_granted';
  data: {};
}

export interface NotificationsPermissionDeniedAnalyticsEvent {
  type: 'notifications_permission_denied';
  data: {};
}

export interface WebChannelShareLinkOpenAnalyticsEvent {
  type: 'web_channel_share_link_open';
  data: {
    shareId: string;
  };
}

export interface WebCollectionShareLinkOpenAnalyticsEvent {
  type: 'web_collection_share_link_open';
  data: {
    shareId: string;
  };
}

export interface WebCollectionReadMoreAnalyticsEvent {
  type: 'web_collection_read_more';
  data: {
    channelId: string;
  };
}

export interface CloseFollowChannelModalAnalyticsEvent {
  type: 'close_follow_channel_modal';
  data: {};
}

export interface PostFollowClickAppPromotionShowAnalyticsEvent {
  type: 'web_after_follow_click_app_promo';
  data:
    | {
        channelId: string;
        channelName: string;
      }
    | {
        groupId: string;
        groupName: string;
      };
}

export interface PostFollowClickAppPromotionChooseWebAnalyticsEvent {
  type: 'web_after_follow_click_app_choose_web';
  data:
    | {
        channelId: string;
        channelName: string;
      }
    | {
        groupId: string;
        groupName: string;
      };
}

export interface PostFollowClickAppPromotionChooseAppAnalyticsEvent {
  type: 'web_after_follow_click_app_choose_app';
  data:
    | {
        channelId: string;
        channelName: string;
      }
    | {
        groupId: string;
        groupName: string;
      };
}

export interface GroupShareAnalyticsEvent {
  type: 'group_share';
  data: {
    groupId: string;
  };
}

export interface OpenChannelFromGroupAnalyticsEvent {
  type: 'open_chanel_from_group';
  data: {
    channelId: string;
    groupId: string;
  };
}

export interface OnBoardingOpen {
  type: 'onboarding_open';
  data: {
    groupId?: string;
  };
}

export interface OnBoardingFinished {
  type: 'onboarding_finished';
  data: {};
}

export interface OnBoardingSkip {
  type: 'onboarding_skip';
  data: {};
}

export interface OnBoardingNext {
  type: 'onboarding_next';
  data: {
    to: number;
  };
}

export interface OnBoardingBack {
  type: 'onboarding_back';
  data: {
    to: number;
  };
}

export interface AdClick {
  type: 'ad_click';
  data: {
    ad_name: 'cinema-tlv';
  };
}

export type AnalyticsEvent =
  | NotificationOpenChannelAnalyticsEvent
  | NotificationListNotificationOpenAnalyticsEvent
  | PushNotificationInteractionAnalyticsEvent
  | InAppNotificationInteractionAnalyticsEvent
  | NotificationSettingsChangedAnalyticsEvent
  | ChannelSubscribeAnalyticsEvent
  | ChannelUnsubscribeAnalyticsEvent
  | ChannelShareAnalyticsEvent
  | ChannelNotificationSettingsChangedAnalyticsEvent
  | ChannelDeliverySettingsUpdatedAnalyticsEvent
  | BarCodeReadAnalyticsEvent
  | NotificationShareChannelAnalyticsEvent
  | FeaturedChannelClickAnalyticsEvent
  | StoreSelectedCategoryAnalyticsEvent
  | AppLinkOpenAnalyticsEvent
  | AppFirstRunAnalyticsEvent
  | SearchResultClickAnalyticsEvent
  | SearchPagingAnalyticsEvent
  | SearchAnalyticsEvent
  | ChannelSampleNotificationClickedAnalyticsEvent
  | NotificationsMarkAllReadAnalyticsEvent
  | SwitchToAppAnalyticsEvent
  | DontSwitchToAppAnalyticsEvent
  | AskNotificationPermissionNativeAnalyticsEvent
  | AskNotificationPermissionFakeAnalyticsEvent
  | AskNotificationPermissionFakeV1AnalyticsEvent
  | AskNotificationPermissionFakeV2AnalyticsEvent
  | NotificationsPermissionDeniedAnalyticsEvent
  | NotificationsPermissionGrantedAnalyticsEvent
  | WebNotificationsNotSupportedAnalyticsEvent
  | WebChannelShareLinkOpenAnalyticsEvent
  | PostFollowClickAppPromotionChooseAppAnalyticsEvent
  | PostFollowClickAppPromotionShowAnalyticsEvent
  | WebCollectionShareLinkOpenAnalyticsEvent
  | PostFollowClickAppPromotionChooseWebAnalyticsEvent
  | WebCollectionReadMoreAnalyticsEvent
  | CloseFollowChannelModalAnalyticsEvent
  | GroupShareAnalyticsEvent
  | OpenChannelFromGroupAnalyticsEvent
  | OnBoardingOpen
  | OnBoardingBack
  | OnBoardingSkip
  | OnBoardingNext
  | OnBoardingFinished
  | AdClick;
