export interface BlackListHoursConf {
  [day: string]: string[];
}
