type PublicVisibility = 'public';
type PrivateVisibility = 'private';
type SecretVisibility = 'secret';

type ChannelVisibilityOptions = PublicVisibility | PrivateVisibility | SecretVisibility;
