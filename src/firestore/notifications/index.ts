import f from 'firebase';
import { NotificationActions, NotificationButton } from '../../common/notifications';

interface NotificationDocBase {
  iconUrl?: string;
  id: string;
  imageUrl?: string;
  imageThumbnailUrl?: string;
  text: string;
  title?: string;
  channel: { id: string; name: string; visibility: ChannelVisibilityOptions };
  actions?: NotificationActions;
  createdAt: f.firestore.Timestamp;
}

export interface SubscriberNotificationDoc extends NotificationDocBase {
  lastReadAt: null | f.firestore.Timestamp;
  scheduledFor?: f.firestore.Timestamp;
  buttons?: NotificationButton[];
}

export interface ManagerNotificationDoc extends NotificationDocBase {
  tag?: string;
  renotify: boolean;
  silent: boolean;
  requireInteraction: boolean;
  buttons: NotificationButton[];
  analytics?: {
    events?: {
      press?: number;
      actionPress?: number;
      delivered?: number;
      sent?: number;
      expand?: number;
      dismissed?: number;
    };
    buttons?: {
      [actionButtonId: string]: number;
    };
    texts?: [{ subscriberId: string; text: string; timestamp: number }];
  };
}

export interface ManagerNotificationDraftDoc extends Omit<ManagerNotificationDoc, 'channel'> {
  suggestedChannels?: string[];
}
