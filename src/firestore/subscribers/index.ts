import f from 'firebase';
import { BlackListHoursConf } from '../../common/subscribers';

export interface SubscriberDoc {
  firebaseUserId: string;
  id: string;
  createdAt: f.firestore.Timestamp;
  deliverySettings?: {
    groupAll?: boolean;
    deliverSilently?: boolean;
    daysBlackList?: BlackListHoursConf | null;
  };
}
