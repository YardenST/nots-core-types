import f from 'firebase';

export interface ManagerDoc {
  apiToken: string;
  firebaseUserId: string;
  testChannelId?: string;
  id: string;
  createAt: f.firestore.Timestamp;
}
