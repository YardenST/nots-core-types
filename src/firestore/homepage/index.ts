export interface HomepageChannel {
  description?: string;
  name: string;
  id: string;
  iconUrl?: string;
}

export interface HomepageCategory {
  name: string;
  order: number;
  channels: HomepageChannel[];
}

export interface HomepageSectionDoc {
  order: number;
  categories: { [categoryName: string]: HomepageCategory };
}

export interface HomepageDocs {
  [sectionId: string]: HomepageSectionDoc;
}
