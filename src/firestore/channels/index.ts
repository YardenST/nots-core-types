import f from 'firebase';
import { BlackListHoursConf } from '../../common/subscribers';
import { NotificationButton } from '../../common/notifications';

export interface NotificationSnippet {
  createdAt: f.firestore.Timestamp;
  iconUrl?: string;
  imageUrl?: string;
  imageThumbnailUrl?: string;
  id: string;
  text: string;
  title?: string;
  buttons?: NotificationButton[];
}

export interface PublicChannelStats {
  notificationsCount: number;
  freq: number;
}

export interface BaseChannelDoc {
  description?: string;
  iconUrl?: string;
  badgeUrl?: string;
  followCustomText?: string;
  id: string;
  name: string;
  requiresExplicitApproval: boolean;
  slug: string;
  shareLink?: string;
  downloadLink?: string;
  tags?: string[];
  visibility: ChannelVisibilityOptions;
  createdAt: f.firestore.Timestamp;
  updatedAt: f.firestore.Timestamp;
}

export interface PublicChannelDoc extends BaseChannelDoc {
  notificationsHistory?: NotificationSnippet[];
  stats: PublicChannelStats;
  passwordProtected?: boolean;
  subscribersCount?: number;
}

export interface SubscriberChannelDoc extends BaseChannelDoc {
  joinedAt: f.firestore.Timestamp;
  isActive?: boolean;
  deliverySettings?: {
    daysBlackList?: BlackListHoursConf | null;
    groupAll?: boolean | null;
    deliverSilently?: boolean | null;
  };
}

export interface ManagerChannelDoc extends BaseChannelDoc {
  subscribersCount: number;
  passwordProtected?: boolean;
}
