import f from 'firebase';

export interface BaseGroupDoc {
  description?: string;
  shareText?: string;
  iconUrl?: string;
  coverUrl?: string;
  id: string;
  hideIcon?: boolean;
  categories?: string[];
  name: string;
  direction?: 'rtl' | 'ltr';
  slug: string;
  channels: PublicGroupChannel[];
  shareLink?: string;
  downloadLink?: string;
  promotionText?: string;
  contactDetails?: { email?: string; phone?: string; address?: string };
  createdAt: f.firestore.Timestamp;
  updatedAt: f.firestore.Timestamp;
  onBoardingAssets?: Record<string, string>;
}

export interface PublicGroupChannel {
  channelId: string;
  order: number;
  category?: string;
  name?: string;
  iconUrl?: string;
}

export interface PublicGroupDoc extends BaseGroupDoc {}

export interface ManagerGroupDoc extends BaseGroupDoc {}
