import { ChannelStats } from '../channels';

export interface CreateChannelSearchRequestBody {
  text: string;
  skip?: number;
}

export interface ChannelSearchResult {
  tags?: string[];
  requiresExplicitApproval: boolean;
  _id: string;
  name: string;
  slug: string;
  createdAt: string;
  updatedAt: string;
  shareLink?: string;
  downloadLink?: string;
  description?: string;
  iconUrl?: string;
  stats?: ChannelStats;
  score: number;
}

export interface CreateChannelSearchResponseBody {
  count: number;
  skip: number;
  pageSize: number;
  results: ChannelSearchResult[];
}
