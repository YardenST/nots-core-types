import { ChannelResponseBody } from '../channels';

export interface CreateManagerRequestBody {}

export interface CreateManagerResponseBody {
  apiToken: string;
  firebaseUserId: string;
  createdAt: string;
  updatedAt: string;
  _id: string;
}

export interface CreateManagerTestChannelRequestBody {}

export interface CreateManagerTestChannelResponseBody extends ChannelResponseBody {}
