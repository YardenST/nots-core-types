import { NotificationData } from '../notifications';

export interface RegisterAddOnRequestBody {
  channelId: string;
  addOnId: string;
}

export interface RegisterAddOnResponseBody {
  isRegistered: boolean;
  addOnId: string;
  manageUrl: string;
}

export interface UnregisterAddOnRequestBody {
  channelId: string;
  addOnId: string;
}

export interface UnregisterAddOnResponseBody {
  isRegistered: boolean;
  addOnId: string;
}

export interface ManageAddOnRequestBody {
  channelId: string;
}

export interface ManageAddOnResponseBody {
  url: string;
}

export interface CreateNotificationRequestBody extends NotificationData {
  channelId: string;
}

export interface CreateNotificationResponseBody extends NotificationData {
  _id: string;
  channelId: string;
  createdAt: string;
  updatedAt: string;
  channelSubsCount: number;
}
