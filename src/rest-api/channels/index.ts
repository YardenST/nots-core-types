export interface CreateChannelRequestBody {
  name: string;
  description?: string;
  iconUrl?: string;
  password?: string;
  visibility?: ChannelVisibilityOptions;
  slug?: string;
}

export interface ChannelStats {
  freq: number;
  notificationsCount: number;
  lastNotificationDate: string;
}

export interface ChannelResponseBody {
  name: string;
  slug: string;
  managerId: string;
  iconUrl?: string;
  badgeUrl?: string;
  followCustomText?: string;
  description?: string;
  isActive: boolean;
  subscribersCount: number;
  webhookConfiguration?: ChannelWebhookConfiguration;
  stats?: ChannelStats;
  tags?: string[];
  nameTags: string;
  requiresExplicitApproval: boolean;
  visibility: ChannelVisibilityOptions;
  shareLink?: string;
  downloadLink?: string;
  _id: string;
  createdAt: string;
  updatedAt: string;
}

export interface CreateChannelResponseBody extends ChannelResponseBody {}

interface ChannelWebhookConfiguration {
  url: string;
  username?: string;
  password?: string;
}

export interface ModifyChannelRequestBody {
  description?: string;
  iconUrl?: string;
  badgeUrl?: string;
  followCustomText?: string;
  tags?: string[];
  visibility?: ChannelVisibilityOptions;
  requiresExplicitApproval?: boolean;
  webhookConfiguration?: ChannelWebhookConfiguration;
}

export interface ModifyChannelResponseBody extends ChannelResponseBody {}

export interface DeleteChannelResponseBody {
  delete: boolean;
}
