export interface CreateChannelSuggestionRequestBody {
  text: string;
  searchText: string;
}

export interface CreateChannelSuggestionResponseBody extends ChannelSuggestion {}

export interface ResolveChannelSuggestionRequestBody {
  channelId: string;
}

export interface ResolveChannelSuggestionResponseBody {
  success: boolean;
}

export interface RejectChannelSuggestionRequestBody {
  reason?: string;
}

export interface RejectChannelSuggestionResponseBody {
  success: boolean;
}

interface ChannelSuggestion {
  _id: string;
  text: string;
  searchText: string;
  subscriberId: string;
  createdAt: string;
  updatedAt: string;
  resolved: boolean;
  pending: boolean;
  editorMessage?: string;
}

export interface GetChannelSuggestionsResponseBody {
  results: ChannelSuggestion[];
}
