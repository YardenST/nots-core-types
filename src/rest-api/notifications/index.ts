import { NotificationActions, NotificationButton } from '../../common/notifications';

type override = { title?: string; text?: string };

export interface NotificationData {
  text: string;
  timestamp?: number;
  tag?: string;
  title?: string;
  iconUrl?: string;
  imageUrl?: string;
  silent?: boolean;
  requireInteraction?: boolean;
  actions?: NotificationActions;
  renotify?: boolean;
  buttons?: NotificationButton[];
  useIconAsPreview?: boolean;
  ttl?: number;
  guid?: string;
  platformOverrides?: {
    ios?: override;
    android?: override;
    web?: override;
  };
}

export interface CreateNotificationDraftRequestBody extends NotificationData {}

export interface CreateNotificationDraftResponseBody extends NotificationData {
  _id: string;
  managerId: string;
  createdAt: string;
  updatedAt: string;
}

export interface ModifyNotificationDraftRequestBody extends CreateNotificationDraftRequestBody {}

export interface ModifyNotificationDraftResponseBody extends CreateNotificationDraftResponseBody {}

export interface DeleteNotificationDraftResponseBody extends NotificationData {
  success: boolean;
}

export interface CreateNotificationRequestBody extends NotificationData {
  channelId: string;
}

export interface CreateNotificationResponseBody extends NotificationData {
  channelId: string;
  replyToSubscriberId?: string;
  parentNotificationId?: string;
  _id: string;
  createdAt: string;
  updatedAt: string;
}

interface NotificationEventPayloadBase {
  timestamp: number;
  subscriberId: string;
}

interface NotificationActionPressEventPayload extends NotificationEventPayloadBase {
  buttonId: string;
  text?: string | null;
}

interface NotificationDismissedEventPayload extends NotificationEventPayloadBase {}

interface NotificationDeliveredEventPayload extends NotificationEventPayloadBase {}

interface NotificationPressEventPayload extends NotificationEventPayloadBase {
  counter: number;
}

export type NotificationWebhookPayload =
  | NotificationPressEventPayload
  | NotificationDismissedEventPayload
  | NotificationActionPressEventPayload
  | NotificationDeliveredEventPayload;

export type NotificationWebhookEventName = 'actionPress' | 'dismissed' | 'press' | 'delivered' | 'expand';

export interface CreateNotificationWebhookRequestBody {
  eventName: NotificationWebhookEventName;
  eventPayload: NotificationWebhookPayload;
  subscriberId: string;
  token: string;
}

export interface CreateNotificationWebhookResponseBody {
  success: boolean;
}

export interface ModifySubscriberNotificationRequestBody {
  markAsRead?: boolean;
  clientTimestamp: number;
  token?: string;
  subscriberId?: string;
}

export interface ModifySubscriberNotificationResponseBody {
  success: boolean;
}

export interface SetSubscriberStaleRequestBody {}

export interface SetSubscriberStaleResponseBody {
  success: boolean;
}
