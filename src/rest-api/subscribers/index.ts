import { BlackListHoursConf } from '../../common/subscribers';

interface BlackListHoursMap {
  [day: string]: {
    [time: string]: {
      day: string;
      hour: string;
    };
  };
}

interface DeliverySettings {
  _id: string;
  daysBlackListHoursMapping: BlackListHoursMap;
}

export interface CreateSubscriberRequestBody {
  fcmToken: string;
  clientType?: string;
  clientAgent?: string;
  timezone: string;
}

export interface CreateSubscriberResponseBody {
  isActive: boolean;
  _id: string;
  firebaseUserId: string;
  timezone: string;
  clientType?: string;
  clientAgent?: string;
  createdAt: string;
  updatedAt: string;
  deliverySettings: DeliverySettings | null;
}

export interface ModifyDeliverySettingsRequestBody {
  daysBlackList: BlackListHoursConf;
  groupAll?: boolean;
  deliverSilently?: boolean;
}

export interface ModifyDeliverySettingsResponseBody {
  daysBlackListHoursMapping: BlackListHoursMap;
  createdAt: string;
  updatedAt: string;
}

export interface ModifyChannelDeliverySettingsRequestBody {
  daysBlackList: BlackListHoursConf;
  groupAll?: boolean | null;
  deliverSilently?: boolean | null;
}

export interface ModifyChannelDeliverySettingsResponseBody {
  daysBlackListHoursMapping: BlackListHoursMap;
  createdAt: string;
  updatedAt: string;
}

export interface CreateChannelSubscriptionRequestBody {
  password?: string;
}

export interface CreateChannelSubscriptionResponseBody {
  channelId: string;
  subscriberId: string;
  isActive: boolean;
  deliverySettings?: {
    createdAt: string;
    updatedAt: string;
    daysBlackListHoursMapping: BlackListHoursMap;
  };
  createdAt: string;
  updatedAt: string;
}

export interface MarkNotificationsAsReadRequestBody {}

export interface MarkNotificationsAsReadResponseBody {
  success: boolean;
}

export interface ModifyNotificationRequestBody {
  readAt?: number;
}

export interface ModifyNotificationResponseBody {
  success: boolean;
}
