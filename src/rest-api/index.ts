export interface RequestValidationErrorResponse {
  statusCode: number;
  error: string;
  message: string;
  details: {
    [fieldName: string]: {
      value: any;
      msg: string;
      param: string;
      location: string;
    };
  };
}
