export interface CreateGroupRequestBody {
  name: string;
  description?: string;
  iconUrl?: string;
  coverUrl?: string;
  slug?: string;
}

export interface GroupResponseBody {
  name: string;
  slug: string;
  managerId: string;
  iconUrl?: string;
  hideIcon?: boolean;
  coverUrl?: string;
  channels?: { channelId: string; order: number; category?: string }[];
  description?: string;
  promotionText?: string;
  contactDetails?: { email?: string; phone?: string; address?: string };
  shareText?: string;
  direction?: 'rtl' | 'ltr';
  shareLink?: string;
  downloadLink?: string;
  _id: string;
  createdAt: string;
  updatedAt: string;
  onBoardingAssets?: Record<string, string>;
}

export interface CreateGroupResponseBody extends GroupResponseBody {}

export interface ModifyGroupRequestBody {
  description?: string;
  shareText?: string;
  name?: string;
  direction?: 'rtl' | 'ltr';
  iconUrl?: string;
  slug?: string;
  hideIcon?: boolean;
  coverUrl?: string;
  promotionText?: string;
  contactPhone?: string;
  contactEmail?: string;
  contactAddress?: string;
}

export interface ModifyGroupItemsRequestBody {
  channels: { channelId: string; category?: string }[];
}

export interface ModifyGroupItemsResponseBody extends GroupResponseBody {}

export interface ModifyGroupResponseBody extends GroupResponseBody {}

export interface ModifyGroupItemRequestBody {
  category?: string;
}

export interface ModifyGroupItemResponseBody extends GroupResponseBody {}

export interface ModifyOnBoardingAssetRequestBody {
  assetValue: string;
}

export interface ModifyOnBoardingAssetResponseBody extends GroupResponseBody {}

export interface DeleteGroupResponseBody {
  delete: boolean;
}
