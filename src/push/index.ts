export interface NotificationPayloadData {
  title: string;
  firebaseUserId: string;
  token: string;
  channelVisibility: ChannelVisibilityOptions;
  body: string;
  renotify: string;
  silent: string;
  buttons?: string;
  requireInteraction: boolean;
  imageUrl: string;
  iconUrl: string;
  badgeUrl: string;
  useIconAsPreview?: string;
  notsNotificationId: string;
  timestamp?: string;
  channelId: string;
  threadId: string;
  channelName: string;
  actions?: string;
  tag: string;
  isNoplUpdatedNotification?: string;
}

export interface NotificationButton {
  text: string;
  id: string;
  type: 'button' | 'text';
  placeholder?: string;
}
